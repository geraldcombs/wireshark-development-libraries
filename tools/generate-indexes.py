#!/usr/bin/env python3
#
# SPDX-License-Identifier: GPL-2.0-or-later
'''Generate Apache / Nginx style directory indexes.
'''

import hashlib
import os

from string import Template

IDX_HEADER = Template('''\
<!DOCTYPE html>
<html><head>
    <meta charset="utf-8">
    <title>Index of $path</title>
  </head><body>
    <pre>
Index of $path
''')

IDX_LINK = Template('<a href="$file">$file</a>')

IDX_FOOTER='''\
    </pre>
</body></html>
'''

HASH_EXTENSIONS = ['.exe', '.zip']
HASH_NAMES = ['SHA256', 'SHA512']
BUF_SIZE = 1 * 1024 * 1024

def index_dir(sys_path, rel_path=None):
    if not rel_path:
        rel_path = '.'
        print(f'Starting at {sys_path}')

    idx_path = os.path.join(sys_path, rel_path)
    path_name = os.path.normpath(os.path.join('/', rel_path))

    print(f'Indexing {rel_path}')

    with os.scandir(idx_path) as dir_it:
        entries = list(dir_it)

    entries = [e for e in entries if e.name != 'index.html']
    entries.sort(key=lambda x: x.name)
    # Directories first, files in reverse order
    dir_entries = [e for e in entries if e.is_dir()]
    file_entries = [e for e in entries if not e.is_dir()]
    file_entries.reverse()
    entries = dir_entries + file_entries

    # XXX Make the output YAML?
    with open(os.path.join(idx_path, 'index.html'), 'w') as idx_f:
        idx_f.write(IDX_HEADER.substitute(path=path_name))
        if rel_path != '.':
            idx_f.write(IDX_LINK.substitute(file='..') + '\n')
        for entry in entries:
            idx_f.write(IDX_LINK.substitute(file=entry.name) + '\n')
            if entry.is_dir():
                index_dir(sys_path, os.path.join(rel_path, entry.name))
            elif os.path.splitext(entry.name)[1] in HASH_EXTENSIONS:
                with open(os.path.join(idx_path, entry.name), 'rb') as hash_f:
                    hashes = {}
                    for hash_name in HASH_NAMES:
                        hashes[hash_name] = hashlib.new(hash_name.lower())
                    while 1:
                        buf = hash_f.read(BUF_SIZE)
                        if not buf:
                            break;
                        for hash in hashes:
                            hashes[hash].update(buf)
                    for hash_name in HASH_NAMES:
                        idx_f.write(f'  {hash_name}: {hashes[hash_name].hexdigest()}\n')

        idx_f.write(IDX_FOOTER)


def main():
    this_dir = os.path.dirname(__file__)
    index_dir(os.path.join(this_dir, '..', 'public'))

if __name__ == '__main__':
    main()
